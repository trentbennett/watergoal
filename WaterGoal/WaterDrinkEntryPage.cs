﻿using System;

using Xamarin.Forms;

namespace WaterGoal
{
	public class WaterDrinkEntryPage : ContentPage
	{
		private WaterGoalPage mParent;
		private WaterGoalDatabase mDatabase;

		public WaterDrinkEntryPage (WaterGoalPage parent, WaterGoalDatabase database)
		{
			mParent = parent;
			mDatabase = database;

			Title = "How much did you drink?";

			var entry = new Entry {
				Placeholder = "Amount"
			};
			var picker = new Picker {
				Title = "Unit"
			};
			picker.Items.Add (Units.FL_OZ);
			picker.Items.Add (Units.LITERS);
			picker.Items.Add (Units.GALLONS);
			var button = new Button {
				Text = "Add"
			};

			button.Clicked += async (sender, e) => {
				var waterAmount = Convert.ToDouble(entry.Text);

				mDatabase.AddWaterDrink(waterAmount, picker.Items[picker.SelectedIndex]);

				await Navigation.PopAsync();

				mParent.Refresh();
			};

			Content = new StackLayout { 
				Spacing = 20,
				Padding = new Thickness(20),
				Children = { entry, picker, button },
			};
		}
	}
}


