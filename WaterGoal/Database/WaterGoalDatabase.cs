﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace WaterGoal
{
	public class WaterGoalDatabase
	{
		private SQLiteConnection connection;

		public WaterGoalDatabase ()
		{
			connection = DependencyService.Get<ISQLite> ().GetConnection ();
			connection.CreateTable<WaterDrink> ();
		}

		public IEnumerable<WaterDrink> GetWaterDrinks() {
			return (from t in connection.Table<WaterDrink> ()
				select t).ToList ();
		}

		public WaterDrink GetWaterDrink(int id) {
			return connection.Table<WaterDrink> ().FirstOrDefault (t => t.ID == id);
		}

		public void DeleteWaterDrink(int id) {
			connection.Delete<WaterDrink> (id);
		}

		public void AddWaterDrink(double amount, string unit) {
			var newThought = new WaterDrink {
				Amount = amount,
				Unit = unit,
				DrankOn = DateTime.Now
			};

			connection.Insert (newThought);
		}
	}
}

