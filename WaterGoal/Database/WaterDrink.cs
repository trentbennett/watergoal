﻿using System;
using SQLite.Net.Attributes;

namespace WaterGoal
{
	public class WaterDrink
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public double Amount { get; set; }
		public String Unit { get; set; }
		public DateTime DrankOn { get; set; }

		public WaterDrink ()
		{
		}
			
	}
}

