﻿using System;
using SQLite.Net;

namespace WaterGoal
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

