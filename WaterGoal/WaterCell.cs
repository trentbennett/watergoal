﻿using System;
using Xamarin.Forms;

namespace WaterGoal
{
	public class WaterCell : ViewCell
	{
		Label amountLabel, unitLabel, dateLabel;

//		public static readonly BindableProperty AmountProperty =
//			BindableProperty.Create ("Amount", typeof(double), typeof(WaterCell), "");
//		public static readonly BindableProperty UnitProperty =
//			BindableProperty.Create ("Unit", typeof(String), typeof(WaterCell), 0);
//		public static readonly BindableProperty DateProperty =
//			BindableProperty.Create ("DrankOn", typeof(DateTime), typeof(WaterCell), new DateTime());

//		public string Amount {
//			get { return(string)GetValue (AmountProperty); }
//			set { SetValue (AmountProperty, value); }
//		}
		/*public string Unit {
			get { return(string)GetValue (UnitProperty); }
			set { SetValue (AmountProperty, value); }
		}
		public string Date {
			get { return(string)GetValue (DateProperty); }
			set { SetValue (AmountProperty, value); }
		}*/

		
		public WaterCell ()
		{
			StackLayout parentLayout = new StackLayout ();
			StackLayout horizontialLayout = new StackLayout ();
			amountLabel = new Label();
			unitLabel = new Label();
			dateLabel = new Label();


			amountLabel.SetBinding (Label.TextProperty, "Amount");
			unitLabel.SetBinding (Label.TextProperty, "Unit");
			dateLabel.SetBinding (Label.TextProperty, "DrankOn");

			horizontialLayout.Orientation = StackOrientation.Horizontal;
			horizontialLayout.Children.Add (amountLabel);
			horizontialLayout.Children.Add (unitLabel);
			parentLayout.Children.Add (horizontialLayout);
			parentLayout.Children.Add (dateLabel);

			View = parentLayout;
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();

//			if (BindingContext != null) {
//				amountLabel.Text = Amount.ToString ();
//				//unitLabel.Text = Unit;
//				//dateLabel.Text = Date.ToString ();
//			}
		}
	}
}

