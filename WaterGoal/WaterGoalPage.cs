﻿using System;

using Xamarin.Forms;

namespace WaterGoal
{
	public class WaterGoalPage : ContentPage
	{
		private WaterGoalDatabase database;
		private ListView waterGoalList;


		public WaterGoalPage (WaterGoalDatabase database)
		{
			this.database = database;
			Title = "Water Goal";
			var waterDrinks = database.GetWaterDrinks ();

			waterGoalList = new ListView ();
			waterGoalList.ItemsSource = waterDrinks;
			waterGoalList.ItemTemplate = new DataTemplate (typeof(WaterCell));

			var toolbarItem = new ToolbarItem {
				Name = "Add",
				Command = new Command(() => Navigation.PushAsync(new WaterDrinkEntryPage(this, database)))
			};

			ToolbarItems.Add (toolbarItem);

			Content = waterGoalList;
		}

		public void Refresh() {
			waterGoalList.ItemsSource = database.GetWaterDrinks();
		}
	}
}


